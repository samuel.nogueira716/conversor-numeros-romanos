var letras_romanos = ['I','V','X','L','C','D','M']
var valores_romanos = [1,5,10,50,100,500,1000]
function converter(){
    var valor = document.querySelector(".input_romano").value
    var letras = valor.split("")
    var total = 0
    for(i=0;i<letras.length;i++){
        var letra = letras[i]
        var letra_seguinte = letras[i+1] || ""
        if(letras_romanos.indexOf(letra) != -1){
            if(letras_romanos.indexOf(letra) < letras_romanos.indexOf(letra_seguinte)){
                var posicao_valor = letras_romanos.indexOf(letra)
                total = total - valores_romanos[posicao_valor]
            }else{
                var posicao_valor = letras_romanos.indexOf(letra)
                total = total + valores_romanos[posicao_valor]
            }
        }
    }
    document.querySelector(".input_resposta").value = total
}

